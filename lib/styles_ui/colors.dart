import 'dart:ui';

const Color cBgColor = Color(0xFFecf5fb);
const Color cOrangeColor = Color(0xFFF07662);
const Color cDarkGreyColor = Color(0xFF727C9B);
