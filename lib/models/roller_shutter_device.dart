import 'package:smart_home/models/devices_types.dart';

class RollerShutterDevices extends DevicesTypes {
  final int? position;
  const RollerShutterDevices({
    required this.position,
    required super.deviceName,
    required super.id,
    required super.productType,
  });
}
