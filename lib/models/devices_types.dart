import 'package:flutter/foundation.dart' show immutable;
/*Adding immutable to indicate that class won't be modified */
@immutable
/* Create an abstract class that will serve as blueprint for subclass that'll will exentd it*/
abstract class DevicesTypes {
  /*Defining the base structure of each future sublass */
  final String? deviceName;
  final int? id;
  final String? productType;
  final bool? mode;
  
  const DevicesTypes({
    this.mode,
    required this.deviceName,
    required this.id,
    required this.productType,
  });
}




