import 'package:smart_home/models/devices_types.dart';

class HeaterDevices extends DevicesTypes {
  final int? temperature;

  const HeaterDevices({
    required this.temperature,
    required super.mode,
    required super.deviceName,
    required super.id,
    required super.productType,
  });
}
