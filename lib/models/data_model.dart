import 'package:smart_home/models/user_api_model.dart';
import 'package:smart_home/models/devices_api_model.dart';

class DataModel {
  List<Devices>? devices;
  User? user;
  String? error;

  DataModel({this.devices, this.user});
  DataModel.fromJson(Map<String, dynamic> json) {
    if (json['devices'] != null) {
      devices = <Devices>[];
      json['devices'].forEach((v) {
        devices!.add(Devices.fromJson(v));
      });
    }
    user = json['user'] != null ? User.fromJson(json['user']) : null;
  }

  DataModel.withError(String errorMessage) {
    error = errorMessage;
  }

}
