class Devices {
  final int? id;
  final String? deviceName;
  final int? intensity;
  final String? mode;
  final String? productType;
  final int? position;
  final int? temperature;

  Devices({
    required this.id,
    required this.deviceName,
    required this.productType,
    this.intensity,
    this.mode,
    this.position,
    this.temperature,
  });

  factory Devices.fromJson(Map<String, dynamic> json) {
    return Devices(
      id: json['id'],
      deviceName: json['deviceName'],
      intensity: json['intensity'],
      mode: json['mode'],
      productType: json['productType'],
      position: json['position'],
      temperature: json['temperature'],
    );
  }
}
