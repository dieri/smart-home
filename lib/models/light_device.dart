import 'package:smart_home/models/devices_types.dart';

class LightDevices extends DevicesTypes {
  final int? intensity;

  const LightDevices({
    required this.intensity,
    required super.deviceName,
    required super.mode,
    required super.id,
    required super.productType,
  });
}
