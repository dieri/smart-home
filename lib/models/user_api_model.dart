
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart' show immutable;

@immutable
class User {
  String? firstName;
  String? lastName;
  Address? address;
  int? birthDate;

  User({this.firstName, this.lastName, this.address, this.birthDate});

  User.fromJson(Map<String, dynamic> json) {
    firstName = json['firstName'];
    lastName = json['lastName'];
    address =
        json['address'] != null ? Address.fromJson(json['address']) : null;
    birthDate = json['birthDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['firstName'] = firstName;
    data['lastName'] = lastName;
    if (address != null) {
      data['address'] = address!.toJson();
    }
    data['birthDate'] = birthDate;
    return data;
  }
}
/* CREATING OWN CLASS ADDRESS, BECAUSE OF THE MANY FIELD AN OBJECT CLASS HAVE*/
class Address {
  /*ADDING QUESTIONS MARK FOR THE NULL SAFETY, EVEN THOUGH WE'RE ONLY FETCHING 1 Adress*/
  String? city;
  int? postalCode;
  String? street;
  String? streetCode;
  String? country;

  /*CREATE CONSTRUCTORS*/
  Address(
      {this.city, this.postalCode, this.street, this.streetCode, this.country});


  Address.fromJson(Map<String, dynamic> json) {
    city = json['city'];
    postalCode = json['postalCode'];
    street = json['street'];
    streetCode = json['streetCode'];
    country = json['country'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['city'] = city;
    data['postalCode'] = postalCode;
    data['street'] = street;
    data['streetCode'] = streetCode;
    data['country'] = country;
    return data;
  }
}
