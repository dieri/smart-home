import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:smart_home/models/devices_api_model.dart';

/*Function to read Json and then returing it */

Future<List<dynamic>> readJson() async {
  final String response = await rootBundle.loadString('assets/data/');
  final data = await jsonDecode(response);

  return data['devices'].map((data) => Devices.fromJson(data)).toList();
}
