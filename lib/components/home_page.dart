import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_home/api_call/api_call.dart';
import 'package:smart_home/bloc/app_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => DataBloc()..add(DataLoadEvent()),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Smart Home'),
        ),
        body: BlocBuilder<DataBloc, DataState>(
          builder: (context, state) {
            if (state is DataLoadingState) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is DataLoadedState) {
              List<dynamic> devices = state.devicesList;
              return ListView.builder(
                  itemCount: devices.length,
                  itemBuilder: (_, index) {
                    return (Card(
                        color: Colors.blue,
                        elevation: 4,
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ListTile(
                          title: Text(
                            devices[index].deviceName,
                            style: const TextStyle(color: Colors.white),
                          ),
                          subtitle: Text(devices[index].productType,
                              style: const TextStyle(color: Colors.white)),
                          // This trailing comma makes auto-formatting nicer for build methods.assetsa
                          trailing: const Icon(
                            Icons.settings,
                          ),
                        )));
                  });
            }
            return Container();
          },
        ),
      ),
    );
  }
  // return const Scaffold(
  //   backgroundColor: cBgColor,
  //   body: HomePageBody(),
  // );
}
