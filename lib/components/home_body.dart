import 'package:flutter/material.dart';
import 'package:smart_home/styles_ui/colors.dart';

class HomePageBody extends StatefulWidget {
  const HomePageBody({super.key});

  @override
  State<HomePageBody> createState() => _HomePageBodyState();
}

class _HomePageBodyState extends State<HomePageBody> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: size.width * 0.05),
      child: Column(
        children: [
          SizedBox(
            height: size.height * 0.1,
          ),
          const Center(
            child: Text(
              'Welcome into smart home',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 25,
              ),
            ),
          ),
          SizedBox(height: size.height * 0.1),
          const Center(
            child: Text(
              'Your connected devices',
              style: TextStyle(color: cDarkGreyColor, fontSize: 18),
            ),
          ),
          SizedBox(height: size.height * 0.1),
          Column(
            children: [
              Row(
                children: [
                  Container(
                    height: size.height * 0.105,
                    width: size.width * 0.21,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        (BoxShadow(
                          color: Colors.grey.shade200,
                          blurRadius: 30,
                          offset: const Offset(5,5),
                        ))
                      ],
                    ),
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
