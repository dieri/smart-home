part of 'app_bloc.dart';

@immutable
abstract class DataEvent extends Equatable {
  const DataEvent();
  @override
  List<Object?> get props => [];
}

/* DATA LOADING STATE */
class DataLoadEvent extends DataEvent {}

/* DATA LOADED STATE */
// class DataLoadedState extends DataEvent {}
/* DATA ERROR STATE */

// class DataErrorState extends DataEvent {}
