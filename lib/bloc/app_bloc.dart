import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:smart_home/api_call/api_call.dart';

part 'app_state.dart';
part 'app_event.dart';

class DataBloc extends Bloc<DataEvent, DataState> {
  List<dynamic> devices = [];
  /*Create Bloc that will init a loading state reponsible for the circular loading screen then storing the data api inside this state */
  DataBloc() : super(DataInitialState()) {
    on<DataLoadEvent>(((event, emit) async {
      emit(DataLoadingState());
      devices = await readJson();
      /*Adding Future to fake waiting time for retrieving data */
      Future.delayed(const Duration(seconds: 3));
      emit(DataLoadedState(devices));
    }));
  }
}
