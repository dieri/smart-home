part of 'app_bloc.dart';

@immutable
abstract class DataState extends Equatable {
  const DataState();

  @override
  List<Object> get props => [];
}

class DataInitialState extends DataState {}

class DataLoadingState extends DataState {}

class DataLoadedState extends DataState {
  final List<dynamic> devicesList;
  const DataLoadedState(this.devicesList);

  @override
  List<Object> get props => [];
}

class DataErrorState extends DataState {
  final String? message;
  const DataErrorState(this.message);
}
